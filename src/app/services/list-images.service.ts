import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Image } from '../models/image.model';

@Injectable({
  providedIn: 'root'
})
export class ListImagesService {

  constructor(private http: HttpClient) {
  }

  getJSON(): Observable<Image[]> {
    return this.http.get<Image[]> ('../../assets/list-images.json');
  }
}
