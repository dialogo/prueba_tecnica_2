import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListImagesService } from './list-images.service';

describe('ListImagesService', () => {
  let service: ListImagesService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [IonicModule.forRoot (), HttpClientModule]
    });
    service = TestBed.inject(ListImagesService);
  });

  it('Creación servicio list-images', () => {
    expect(service).toBeTruthy();
  });

  describe ('Debe tener datos', () => {
    it ('Carga array de 4000 elementos', () => {
      const serviceImgList: ListImagesService = TestBed.get (ListImagesService);
      serviceImgList.getJSON ().subscribe (data => {
        expect (Array.isArray (data)).toBeTruthy ();
        expect (data.length).toEqual (4000);
      });
    });
  });
});
