import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'imagenes',
    loadChildren: () => import('./pages/list-images/list-images.module').then(m => m.ListaImgPageModule)
  },
  {
    path: '',
    redirectTo: 'imagenes',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
