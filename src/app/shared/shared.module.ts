import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';

import { FilterPipe } from './pipes/filterPipe';
import { SortPipe } from './pipes/sortPipe';

@NgModule({
  declarations: [
    SortPipe,
    FilterPipe
  ],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports:[
    SortPipe,
    FilterPipe
  ]
})
export class SharedModule { }
