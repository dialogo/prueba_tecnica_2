import { Component, OnInit, ViewChild } from '@angular/core';
import { IonContent, IonInfiniteScroll } from '@ionic/angular';
import { dataImage } from '../../constants/image.const';

import { Image } from '../../models/image.model';

import { ListImagesService } from '../../services/list-images.service';
import { FilterPipe } from '../../shared/pipes/filterPipe';
import { SortPipe } from '../../shared/pipes/sortPipe';

@Component({
  selector: 'app-list-images',
  templateUrl: './list-images.page.html',
  styleUrls: ['./list-images.page.scss']
})
export class ListImagesPage implements OnInit {

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  @ViewChild(IonContent, {static: false}) content: IonContent;

  allItems: Image[] = [];

  addItems = 0;
  items: Image[] = [];
  filterItems: Image[] = [];
  filterText = '';

  isHidden = true;

  totalRecords;
  recordsByPage: number = dataImage.recordsByPage;

  constructor(private listImagesService: ListImagesService, private sortPipe: SortPipe, private filterPipe: FilterPipe) {
    this.getAllItems();
  }

  ngOnInit() {}

  getAllItems(){
    this.listImagesService.getJSON().subscribe(listImages => {
      this.allItems = this.sortPipe.transform(listImages, 'asc', 'id');
      this.filterItems = this.allItems;
      this.totalRecords = this.allItems.length;

      this.loadData();
    });
  }

  loadData(event?) {
    this.items = [...this.items, ...this.filterItems.slice(this.addItems, this.addItems + this.recordsByPage)];
    this.addItems += this.recordsByPage;
    if (event) { event.target.complete (); }
    this.infiniteScroll.disabled = this.addItems >= this.totalRecords;
  }

  filter() {
    this.isHidden = false;

    this.content.scrollToTop(0);

    this.addItems = 0;
    this.items = [];

    this.filterItems = this.filterPipe.transform(this.allItems, this.filterText);
    this.totalRecords = this.filterItems.length;

    this.infiniteScroll.disabled = false;

    this.loadData();
  }
}
