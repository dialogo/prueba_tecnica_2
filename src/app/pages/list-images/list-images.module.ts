import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { ListImagesService } from '../../services/list-images.service';
import { FilterPipe } from '../../shared/pipes/filterPipe';
import { SortPipe } from '../../shared/pipes/sortPipe';

import { ListaImgPageRoutingModule } from './list-images-routing.module';

import { SharedModule } from '../../shared/shared.module';

import { ListImagesPage } from './list-images.page';

import { ImageComponent } from '../../components/image/image.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    ListaImgPageRoutingModule
  ],
  declarations: [
    ImageComponent,
    ListImagesPage
  ],
  providers: [
    SortPipe,
    FilterPipe,
    ListImagesService
  ]
})
export class ListaImgPageModule {}
