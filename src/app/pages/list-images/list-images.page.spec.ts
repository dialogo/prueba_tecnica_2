import { ComponentFixture, fakeAsync, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FilterPipe } from '../../shared/pipes/filterPipe';
import { SortPipe } from '../../shared/pipes/sortPipe';

import { ListImagesPage } from './list-images.page';

import { ListImagesService } from '../../services/list-images.service';

import { HttpClientModule } from '@angular/common/http';

describe ('ListaImgPage', () => {
  let component: ListImagesPage;
  let fixture: ComponentFixture<ListImagesPage>;

  beforeEach (waitForAsync (() => {
    TestBed.configureTestingModule ({
      declarations: [ListImagesPage],
      imports: [IonicModule.forRoot (), HttpClientModule],
      providers: [ListImagesService, SortPipe, FilterPipe]
    }).compileComponents ();

    fixture = TestBed.createComponent (ListImagesPage);
    component = fixture.componentInstance;
    fixture.detectChanges ();
  }));

  it ('Creación componente list-images', () => {
    expect (component).toBeTruthy ();
  });

  describe ('Comprobación de items totales e items por página', () => {
    it ('Carga inicial de items', fakeAsync (() => {
      expect (component.allItems.length).toEqual (4000);
      expect (component.items.length).toEqual (30);
    }));

  });

  describe ('Se aplica filtro de búsqueda por ID/Texto', () => {
    it ('Se obtienen resultados', fakeAsync (() => {
      component.filterText = 'L';
      component.allItems = [
        {id: 1, photo: '', text: 'LOREM'},
        {id: 2, photo: '', text: 'L'},
        {id: 3, photo: '', text: 'OREM'}
      ];
      component.filter ();

      expect (component.items.length).toBeGreaterThan (0);
      expect (component.filterItems.length).toBeGreaterThan (0);
      expect (component.items.length).toBeLessThanOrEqual (component.filterItems.length);
    }));

    it ('Sin resultados: Se muestra mensaje "No hay coincidencias"', fakeAsync (() => {
      const compiled = fixture.debugElement.nativeElement;

      component.filterText = 'X';
      component.allItems = [
        {id: 1, photo: '', text: 'LOREM'},
        {id: 2, photo: '', text: 'L'},
        {id: 3, photo: '', text: 'OREM'}
      ];

      component.filter ();

      expect (component.items.length).toEqual (0);
      expect (component.filterItems.length).toEqual (0);
      expect(compiled.querySelector('h1').textContent).toContain('No hay coincidencias');
    }));

  });
});

